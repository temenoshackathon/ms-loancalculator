package com.temenos.microservice.loancalculator.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Entity for store details such as Currency, Loan type and description.
 *
 * @author ssubashchandar
 */
@Entity(name = "LoanDetailEntity")
@Table(name = "loan_details")
public class LoanDetailEntity implements com.temenos.microservice.framework.core.data.Entity, Serializable {

    private static final long serialVersionUID = 9146136921169669640L;

    @Id
    @Column(name = "LoanType", unique = true, nullable = false, length = 20)
    private String loanType;

    @Column(name = "Description", nullable = false, length = 1000)
    private String description;

    @Column(name = "Currency", nullable = false, length = 5)
    private String currency;

    @Column(name = "CurrencySymbol", nullable = false, length = 3)
    private String currencySymbol;

    public String getLoanType() {
        return loanType;
    }

    public void setLoanType(String loanType) {
        this.loanType = loanType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCurrencySymbol() {
        return currencySymbol;
    }

    public void setCurrencySymbol(String currencySymbol) {
        this.currencySymbol = currencySymbol;
    }
}
