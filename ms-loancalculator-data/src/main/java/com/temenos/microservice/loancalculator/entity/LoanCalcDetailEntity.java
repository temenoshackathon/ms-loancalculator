package com.temenos.microservice.loancalculator.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Entity for store basic details such as minimum principle amount, Interest rate and Terms in month,
 * maximum principle amount, Interest rate and Terms in month to Calculate the Loan amount by selecting the values between the range.
 *
 * @author ssubashchandar
 */
@Entity(name = "LoanCalcDetailEntity")
@Table(name = "loan_calculator_detail")
public class LoanCalcDetailEntity implements com.temenos.microservice.framework.core.data.Entity, Serializable {

    private static final long serialVersionUID = 9146136921169669630L;

    @Id
    @Column(name = "LoanType", unique = true, nullable = false, length = 20)
    private String loanType;

    @Column(name = "MinAmount", nullable = false)
    private BigDecimal minAmount;

    @Column(name = "MaxAmount", nullable = false)
    private BigDecimal maxAmount;

    @Column(name = "MinInterestRate", nullable = false)
    private Integer minInterestRate;

    @Column(name = "MaxInterestRate", nullable = false)
    private Integer maxInterestRate;

    @Column(name = "MinTermsInMonth", nullable = false)
    private Integer minTermsInMonth;

    @Column(name = "MaxTermsInMonth", nullable = false)
    private Integer maxTermsInMonth;

    public String getLoanType() {
        return loanType;
    }

    public void setLoanType(String loanType) {
        this.loanType = loanType;
    }

    public BigDecimal getMinAmount() {
        return minAmount;
    }

    public void setMinAmount(BigDecimal minAmount) {
        this.minAmount = minAmount;
    }

    public BigDecimal getMaxAmount() {
        return maxAmount;
    }

    public void setMaxAmount(BigDecimal maxAmount) {
        this.maxAmount = maxAmount;
    }

    public Integer getMinInterestRate() {
        return minInterestRate;
    }

    public void setMinInterestRate(Integer minInterestRate) {
        this.minInterestRate = minInterestRate;
    }

    public Integer getMaxInterestRate() {
        return maxInterestRate;
    }

    public void setMaxInterestRate(Integer maxInterestRate) {
        this.maxInterestRate = maxInterestRate;
    }

    public Integer getMinTermsInMonth() {
        return minTermsInMonth;
    }

    public void setMinTermsInMonth(Integer minTermsInMonth) {
        this.minTermsInMonth = minTermsInMonth;
    }

    public Integer getMaxTermsInMonth() {
        return maxTermsInMonth;
    }

    public void setMaxTermsInMonth(Integer maxTermsInMonth) {
        this.maxTermsInMonth = maxTermsInMonth;
    }
}
