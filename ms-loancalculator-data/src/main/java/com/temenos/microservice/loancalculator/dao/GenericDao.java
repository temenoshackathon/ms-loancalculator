package com.temenos.microservice.loancalculator.dao;

import com.temenos.microservice.framework.core.data.DataAccessException;
import com.temenos.microservice.framework.core.data.Entity;
import com.temenos.microservice.framework.core.data.sql.BaseSqlDao;
import com.temenos.microservice.framework.core.data.sql.SqlDbDao;

/**
 * Class to create the Dao Instance
 *
 * @author ssubashchandar
 */
public class GenericDao {

    /**
     * Create Dao instance for the given Entity class
     *
     * @param <T>
     * @param clazz
     * @return
     * @throws DataAccessException
     */
    public static <T extends Entity> SqlDbDao<T> getDao(Class<T> clazz) throws DataAccessException {
        return (SqlDbDao<T>) BaseSqlDao.getInstance(clazz).getSqlDao();
    }
}
