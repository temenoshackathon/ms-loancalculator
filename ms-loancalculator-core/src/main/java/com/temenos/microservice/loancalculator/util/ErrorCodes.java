package com.temenos.microservice.loancalculator.util;

/**
 * Class helps to manage the error code and error description used in business response
 *
 * @author ssubashchandar
 */
public class ErrorCodes {

    //Error Codes
    public static final String LC000001_CODE = "LC-000001";
    public static final String LC000002_CODE = "LC-000002";
    public static final String LC000003_CODE = "LC-000003";

    //Error Descriptions
    public static final String LC000001_DESC = "Record Exist Already";
    public static final String LC000002_DESC = "Record Not Found";
    public static final String LC000003_DESC = "Invalid Loan Type, Kindly create record in Loan";

}
