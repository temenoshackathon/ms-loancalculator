package com.temenos.microservice.loancalculator.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.temenos.logger.Logger;
import com.temenos.logger.diagnostics.Diagnostic;
import com.temenos.microservice.framework.core.conf.Environment;
import com.temenos.microservice.framework.core.function.Context;
import com.temenos.microservice.framework.core.function.HttpRequest;
import com.temenos.microservice.framework.core.function.RequestImpl;

/**
 * This class helps to manage the generic methods for logging
 *
 * @author ssubashchandar
 */
public class GenericLogger {

    private static final Diagnostic diagnostic = Logger.forDiagnostic().forComp(Constants.LOGGER_API);
    private static final String HYPHEN = "-";
    private static final String COMMA = ",";

    private static boolean probeLogFlag = false;

    static {
        getProbeLogFlag();
    }

    /**
     * Method will be called from static block to get logging status
     */
    public static void getProbeLogFlag() {
        probeLogFlag = Boolean
                .parseBoolean(Environment.getEnvironmentVariable(Constants.LOGGER_PROBE, "false"));
    }

    public static void logRequest(Context ctx) {
        if (probeLogFlag) {
            try {
                HttpRequest request = (HttpRequest) ctx.getRequest();
                StringBuffer requestString = new StringBuffer(
                        Constants.LOGGER_API_PREFIX + HYPHEN + Constants.LOGGER_REQUEST);
                requestString.append(" { UUID:");
                requestString.append(request.getUUID());
                requestString.append(COMMA);
                requestString.append(" Requested resource: ");
                requestString.append(request.getParams().get("CamelHttpUrl"));
                requestString.append(COMMA);
                requestString.append("OperationId: ");
                requestString.append(request.getOperationId());
                requestString.append(COMMA);
                requestString.append("Body: ");
                requestString.append(request.getBody());
                requestString.append("}");
                diagnostic.info(requestString.toString());
            } catch (Exception e) {
                diagnostic.info("Failed to log " + e.getMessage());
            }
        }
    }

    /**
     * Method to get the UUID from Context
     *
     * @param context
     * @return
     */
    public static String getUUIDFromContext(Context context) {
        String UUID = new String();
        try {
            if (context instanceof HttpRequest) {
                UUID = ((HttpRequest) context.getRequest()).getUUID();
            } else {
                UUID = ((RequestImpl) context.getRequest()).getHeaders().get("UUID").get(0);
            }
        } catch (Exception e) {
            UUID = null;
        }
        return UUID;
    }

    /**
     * Method helps to log the response object
     *
     * @param obj
     * @param UUID
     */
    public static void logResponse(Object obj, String UUID) {
        if (probeLogFlag) {
            try {
                ObjectMapper mapper = new ObjectMapper();
                StringBuffer responseString = new StringBuffer(Constants.LOGGER_API_PREFIX)
                        .append(HYPHEN).append(Constants.LOGGER_RESPONSE);
                responseString.append(" UUID:").append(UUID);
                responseString.append(COMMA);
                responseString.append(mapper.writeValueAsString(obj));
                diagnostic.info(responseString.toString());
            } catch (Exception e) {
                diagnostic.info("Failed to log " + e.getMessage());

            }
        }
    }
}

