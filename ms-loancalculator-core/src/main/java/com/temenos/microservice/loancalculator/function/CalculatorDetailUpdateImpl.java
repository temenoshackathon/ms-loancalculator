package com.temenos.microservice.loancalculator.function;

import com.temenos.microservice.framework.core.FunctionException;
import com.temenos.microservice.framework.core.function.Context;
import com.temenos.microservice.loancalculator.core.CalculatorDetailProcessor;
import com.temenos.microservice.loancalculator.util.GenericLogger;
import com.temenos.microservice.loancalculator.view.LoanCalculatorResponse;

/**
 * Implementation class for updateCalculatorDetail API
 *
 * @author ssubashchandar
 */
public class CalculatorDetailUpdateImpl implements UpdateCalculatorDetail {

    CalculatorDetailProcessor calculatorDetailProcessor;

    /**
     * Override method of updateCalculatorDetail Interface
     *
     * @param context
     * @param input
     * @return
     * @throws FunctionException
     */
    @Override
    public LoanCalculatorResponse invoke(Context context, UpdateCalculatorDetailInput input) throws FunctionException {
        // Log request
        GenericLogger.logRequest(context);
        calculatorDetailProcessor = new CalculatorDetailProcessor();
        LoanCalculatorResponse response = calculatorDetailProcessor.updateCalculatorDetails(context, input);
        // Log response
        GenericLogger.logResponse(response, GenericLogger.getUUIDFromContext(context));
        return response;
    }
}
