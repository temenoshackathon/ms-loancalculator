package com.temenos.microservice.loancalculator.function;

import com.temenos.microservice.framework.core.FunctionException;
import com.temenos.microservice.framework.core.function.Context;
import com.temenos.microservice.loancalculator.core.LoanTypeProcessor;
import com.temenos.microservice.loancalculator.util.GenericLogger;
import com.temenos.microservice.loancalculator.view.LoanCalculatorResponse;

/**
 * Implementation class for createLoanType API
 *
 * @author ssubashchandar
 */
public class LoanTypeCreateImpl implements CreateLoanType {

    private LoanTypeProcessor loanTypeProcessor;

    /**
     * Override method
     *
     * @param context
     * @param input
     * @return
     * @throws FunctionException
     */
    @Override
    public LoanCalculatorResponse invoke(Context context, CreateLoanTypeInput input) throws FunctionException {
        loanTypeProcessor = new LoanTypeProcessor();
        // Log request
        GenericLogger.logRequest(context);
        LoanCalculatorResponse response = loanTypeProcessor.createLoanType(context, input);
        // Log response
        GenericLogger.logResponse(response, GenericLogger.getUUIDFromContext(context));
        return response;
    }
}
