package com.temenos.microservice.loancalculator.function;

import com.temenos.microservice.framework.core.FunctionException;
import com.temenos.microservice.framework.core.function.Context;
import com.temenos.microservice.loancalculator.core.CalculatorDetailProcessor;
import com.temenos.microservice.loancalculator.util.GenericLogger;
import com.temenos.microservice.loancalculator.view.LoanCalculatorResponse;

/**
 * Implementation class for createCalculatorDetail API
 *
 * @author ssubashchandar
 */
public class CalculatorDetailCreateImpl implements CreateCalculatorDetail {

    CalculatorDetailProcessor calculatorDetailProcessor;

    /**
     * Override method of CreateCalculatorDetail Interface
     *
     * @param context
     * @param input
     * @return
     * @throws FunctionException
     */
    @Override
    public LoanCalculatorResponse invoke(Context context, CreateCalculatorDetailInput input) throws FunctionException {
        // Log request
        GenericLogger.logRequest(context);
        calculatorDetailProcessor = new CalculatorDetailProcessor();
        LoanCalculatorResponse response = calculatorDetailProcessor.createCalculatorDetails(context, input);
        // Log response
        GenericLogger.logResponse(response, GenericLogger.getUUIDFromContext(context));
        return response;
    }
}
