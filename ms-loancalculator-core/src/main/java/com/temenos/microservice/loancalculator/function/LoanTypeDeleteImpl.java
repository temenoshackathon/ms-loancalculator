package com.temenos.microservice.loancalculator.function;

import com.temenos.microservice.framework.core.FunctionException;
import com.temenos.microservice.framework.core.function.Context;
import com.temenos.microservice.loancalculator.core.LoanTypeProcessor;
import com.temenos.microservice.loancalculator.util.GenericLogger;
import com.temenos.microservice.loancalculator.view.LoanCalculatorResponse;

/**
 * Implementation class for deleteLoanType API
 *
 * @author ssubashchandar
 */
public class LoanTypeDeleteImpl implements DeleteLoanType {

    private LoanTypeProcessor loanTypeProcessor;

    /**
     * Override method of DeleteLoanType interface
     *
     * @param context
     * @param input
     * @return
     * @throws FunctionException
     */
    @Override
    public LoanCalculatorResponse invoke(Context context, DeleteLoanTypeInput input) throws FunctionException {
        loanTypeProcessor = new LoanTypeProcessor();
        // Log request
        GenericLogger.logRequest(context);
        LoanCalculatorResponse response = loanTypeProcessor.deleteLoanType(context, input);
        // Log response
        GenericLogger.logResponse(response, GenericLogger.getUUIDFromContext(context));
        return response;
    }
}
