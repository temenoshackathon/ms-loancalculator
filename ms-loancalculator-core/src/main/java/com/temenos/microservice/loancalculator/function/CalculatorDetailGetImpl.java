package com.temenos.microservice.loancalculator.function;

import com.temenos.microservice.framework.core.FunctionException;
import com.temenos.microservice.framework.core.function.Context;
import com.temenos.microservice.loancalculator.core.CalculatorDetailProcessor;
import com.temenos.microservice.loancalculator.util.GenericLogger;
import com.temenos.microservice.loancalculator.view.GetCalculatorDetailsBody;
import com.temenos.microservice.loancalculator.view.LoanCalculatorResponse;

/**
 * Implementation class for getCalculatorDetail API
 *
 * @author ssubashchandar
 */
public class CalculatorDetailGetImpl implements GetCalculatorDetail {

    CalculatorDetailProcessor calculatorDetailProcessor;

    /**
     * Override method of GetCalculatorDetail Interface
     *
     * @param context
     * @param input
     * @return
     * @throws FunctionException
     */
    @Override
    public GetCalculatorDetailsBody invoke(Context context, GetCalculatorDetailInput input) throws FunctionException {
        // Log request
        GenericLogger.logRequest(context);
        calculatorDetailProcessor = new CalculatorDetailProcessor();
        GetCalculatorDetailsBody response = calculatorDetailProcessor.getCalculatorDetails(context, input);
        // Log response
        GenericLogger.logResponse(response, GenericLogger.getUUIDFromContext(context));
        return response;
    }
}
