package com.temenos.microservice.loancalculator.core;

import com.temenos.logger.Dimension;
import com.temenos.logger.Logger;
import com.temenos.logger.alert.Alert;
import com.temenos.logger.data.LogCode;
import com.temenos.microservice.framework.core.FunctionException;
import com.temenos.microservice.framework.core.function.Context;
import com.temenos.microservice.loancalculator.entity.LoanCalcDetailEntity;
import com.temenos.microservice.loancalculator.entity.LoanDetailEntity;
import com.temenos.microservice.loancalculator.function.CreateLoanTypeInput;
import com.temenos.microservice.loancalculator.function.DeleteLoanTypeInput;
import com.temenos.microservice.loancalculator.function.GetLoanTypeInput;
import com.temenos.microservice.loancalculator.function.UpdateLoanTypeInput;
import com.temenos.microservice.loancalculator.util.Constants;
import com.temenos.microservice.loancalculator.util.ErrorCodes;
import com.temenos.microservice.loancalculator.util.GenericService;
import com.temenos.microservice.loancalculator.util.Util;
import com.temenos.microservice.loancalculator.view.GetLoanDetail;
import com.temenos.microservice.loancalculator.view.GetLoanTypeBody;
import com.temenos.microservice.loancalculator.view.LoanCalculatorResponse;
import com.temenos.microservice.loancalculator.view.LoanDetailBody;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.temenos.microservice.loancalculator.util.Constants.*;

/**
 * Class helps to create, update, retrieve and delete a loan type from loan details table
 *
 * @author ssubashchandar
 */
public class LoanTypeProcessor {

    private static final Alert alert = Logger.forAlert().forComp(LOGGER_API);

    /**
     * Method helps to create record in loan detail table
     *
     * @param context
     * @param input
     * @return
     */
    public LoanCalculatorResponse createLoanType(Context context, CreateLoanTypeInput input) {
        LoanCalculatorResponse response = new LoanCalculatorResponse();
        try {
            String loanType = input.getParams().get().getLoanType().get(0);
            LoanDetailEntity loanDetail = (LoanDetailEntity) GenericService.fetchEntity(loanType, LoanDetailEntity.class);
            if (loanDetail != null) {
                // returning error response if record exist with inputted loan type
                response.setAdditionalInfo(ErrorCodes.LC000001_DESC);
                response.setReturnCode(ErrorCodes.LC000001_CODE);
            } else {
                loanDetail = new LoanDetailEntity();
                loanDetail.setLoanType(loanType);
                saveLoanDetail(loanDetail, input.getBody());
                response.setAdditionalInfo(CREATE_RECORD_SUCCESS);
            }
        } catch (Exception e) {
            alert.prepareError(new LogCode(PRODUCT, LOGGER_API, Dimension.ALERT, FAILURE_API_PROCESS_CODE, FAILURE_API_PROCESS), e).log();
            response.setAdditionalInfo(Constants.CREATE_RECORD_FAILURE);
        }
        return response;
    }

    /**
     * Method helps tp update the reord in loan detail table
     *
     * @param context
     * @param input
     * @return
     */
    public LoanCalculatorResponse updateLoanType(Context context, UpdateLoanTypeInput input) {
        LoanCalculatorResponse response = new LoanCalculatorResponse();
        try {
            String loanType = input.getParams().get().getLoanType().get(0);
            LoanDetailEntity loanDetail = (LoanDetailEntity) GenericService.fetchEntity(loanType, LoanDetailEntity.class);
            if (loanDetail == null) {
                // returning error response if record exist with inputted loan type
                response.setAdditionalInfo(ErrorCodes.LC000002_DESC);
                response.setReturnCode(ErrorCodes.LC000002_CODE);
            } else {
                saveLoanDetail(loanDetail, input.getBody());
                response.setAdditionalInfo(UPDATE_RECORD_SUCCESS);
            }
        } catch (Exception e) {
            alert.prepareError(new LogCode(PRODUCT, LOGGER_API, Dimension.ALERT, FAILURE_API_PROCESS_CODE, FAILURE_API_PROCESS), e).log();
            response.setAdditionalInfo(Constants.UPDATE_RECORD_FAILURE);
        }
        return response;
    }

    /**
     * Method helps to delete record in loan details table
     *
     * @param context
     * @param input
     * @return
     */
    public LoanCalculatorResponse deleteLoanType(Context context, DeleteLoanTypeInput input) {
        LoanCalculatorResponse response = new LoanCalculatorResponse();
        try {
            String loanType = input.getParams().get().getLoanType().get(0);
            LoanDetailEntity loanDetail = (LoanDetailEntity) GenericService.fetchEntity(loanType, LoanDetailEntity.class);
            if (loanDetail == null) {
                // returning error response if record exist with inputted loan type
                response.setAdditionalInfo(ErrorCodes.LC000002_DESC);
                response.setReturnCode(ErrorCodes.LC000002_CODE);
            } else {
                LoanCalcDetailEntity loanCalcDetail = (LoanCalcDetailEntity) GenericService.fetchEntity(loanType, LoanCalcDetailEntity.class);
                if (loanCalcDetail != null) {
                    GenericService.removeEntity(loanCalcDetail);
                }
                GenericService.removeEntity(loanDetail);
                response.setAdditionalInfo(DELETE_RECORD_SUCCESS);
            }
        } catch (Exception e) {
            alert.prepareError(new LogCode(PRODUCT, LOGGER_API, Dimension.ALERT, FAILURE_API_PROCESS_CODE, FAILURE_API_PROCESS), e).log();
            response.setAdditionalInfo(Constants.DELETE_RECORD_FAILURE);
        }
        return response;
    }

    /**
     * Method helps to retrieve the loan details
     *
     * @param context
     * @param input
     * @return
     */
    public GetLoanTypeBody getLoanType(Context context, GetLoanTypeInput input) {
        GetLoanTypeBody response = new GetLoanTypeBody();
        try {
            List<GetLoanDetail> getLoanDetailList = new ArrayList<>();
            String loanType = input.getParams().get().getLoanType().get(0);
            if (loanType.equalsIgnoreCase("all")) {
                // retrieve all the records from loan detail table and return
                List<LoanDetailEntity> loanDetailEntityList = GenericService.fetchAllLoanType();
                for (LoanDetailEntity loanDetailEntity : loanDetailEntityList) {
                    LoanCalcDetailEntity loanCalcDetail = (LoanCalcDetailEntity) GenericService.fetchEntity(loanDetailEntity.getLoanType(), LoanCalcDetailEntity.class);
                    if(loanCalcDetail != null) {
                        GetLoanDetail loanDetails = new GetLoanDetail();
                        loanDetails.setLoanType(loanDetailEntity.getLoanType());
                        loanDetails.setCurrency(GetLoanDetail.CurrencyEnum.valueOf(loanDetailEntity.getCurrency()));
                        loanDetails.setDescription(loanDetailEntity.getDescription());
                        getLoanDetailList.add(loanDetails);
                    }
                }
                response.setLoanDetails(getLoanDetailList);
            } else {
                LoanDetailEntity loanDetailEntity = (LoanDetailEntity) GenericService.fetchEntity(loanType, LoanDetailEntity.class);
                LoanCalcDetailEntity loanCalcDetail = (LoanCalcDetailEntity) GenericService.fetchEntity(loanType, LoanCalcDetailEntity.class);
                if (loanDetailEntity == null || loanCalcDetail == null) {
                    // returning error response if record exist with inputted loan type
                    response.setAdditionalInfo(ErrorCodes.LC000002_DESC);
                    response.setReturnCode(ErrorCodes.LC000002_CODE);
                } else {
                    // returning on the request loan type details
                    GetLoanDetail loanDetails = new GetLoanDetail();
                    loanDetails.setLoanType(loanDetailEntity.getLoanType());
                    loanDetails.setCurrency(GetLoanDetail.CurrencyEnum.valueOf(loanDetailEntity.getCurrency()));
                    loanDetails.setDescription(loanDetailEntity.getDescription());
                    getLoanDetailList.add(loanDetails);
                    response.setLoanDetails(getLoanDetailList);
                }
            }
        } catch (Exception e) {
            alert.prepareError(new LogCode(PRODUCT, LOGGER_API, Dimension.ALERT, FAILURE_API_PROCESS_CODE, FAILURE_API_PROCESS), e).log();
            response.setAdditionalInfo(FAILURE_API_PROCESS);
        }
        return response;
    }

    /**
     * Method will called from create and update process to store record in table
     *
     * @param loanDetail
     * @param body
     */
    private void saveLoanDetail(LoanDetailEntity loanDetail, Optional<LoanDetailBody> body) throws FunctionException {
        String currency = body.get().getCurrency().toString();
        loanDetail.setCurrency(currency);
        loanDetail.setDescription(body.get().getDescription());
        String currencySymbol = Util.getCurrencySymbol(currency);
        loanDetail.setCurrencySymbol(currencySymbol);
        GenericService.saveEntity(loanDetail);
    }
}
