package com.temenos.microservice.loancalculator.util;

import com.temenos.microservice.framework.core.data.DataAccessException;
import com.temenos.microservice.framework.core.data.Entity;
import com.temenos.microservice.loancalculator.dao.GenericDao;
import com.temenos.microservice.loancalculator.entity.LoanDetailEntity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Class helps to manage the common database queries
 *
 * @author ssubashchandar
 */
public class GenericService {

    /**
     * Method will save the entity object
     *
     * @param entity
     * @throws DataAccessException
     */
    public static void saveEntity(Entity entity) throws DataAccessException {
        GenericDao.getDao(entity.getClass()).save(entity);
    }

    /**
     * Method will return a request row
     *
     * @return
     * @throws DataAccessException
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    public static Object fetchEntity(Object id, Class clazz) throws DataAccessException {
        return GenericDao.getDao(clazz).findById(id, clazz);
    }

    /**
     * Method will delete the inputted entity
     *
     * @return
     * @throws DataAccessException
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    public static void removeEntity(Entity entity) throws DataAccessException {
        GenericDao.getDao(entity.getClass()).deleteById(entity);
    }

    /**
     * Method helps to retrive all record from loan details table
     *
     * @return
     * @throws DataAccessException
     */
    public static List<LoanDetailEntity> fetchAllLoanType() throws DataAccessException {
        String query = new String();
        Map<String, Object> queryParam = new HashMap<String, Object>();
        query = "select lt from LoanDetailEntity lt";
        List<LoanDetailEntity> loanDetailEntityList =
                GenericDao.getDao(LoanDetailEntity.class).executeQuery(query, queryParam, LoanDetailEntity.class);
        return loanDetailEntityList;
    }

}
