package com.temenos.microservice.loancalculator.function;

import com.temenos.microservice.framework.core.FunctionException;
import com.temenos.microservice.framework.core.function.Context;
import com.temenos.microservice.loancalculator.core.LoanTypeProcessor;
import com.temenos.microservice.loancalculator.util.GenericLogger;
import com.temenos.microservice.loancalculator.view.GetLoanTypeBody;
import com.temenos.microservice.loancalculator.view.LoanCalculatorResponse;

/**
 * Implementation class for getLoanType API
 *
 * @author ssubashchandar
 */
public class LoanTypeGetImpl implements GetLoanType {

    private LoanTypeProcessor loanTypeProcessor;

    /**
     * Override method of GetLoanType Interface
     *
     * @param context
     * @param input
     * @return
     * @throws FunctionException
     */
    @Override
    public GetLoanTypeBody invoke(Context context, GetLoanTypeInput input) throws FunctionException {
        loanTypeProcessor = new LoanTypeProcessor();
        // Log request
        GenericLogger.logRequest(context);
        GetLoanTypeBody response = loanTypeProcessor.getLoanType(context, input);
        // Log response
        GenericLogger.logResponse(response, GenericLogger.getUUIDFromContext(context));
        return response;
    }
}
