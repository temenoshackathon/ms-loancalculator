package com.temenos.microservice.loancalculator.util;

/**
 * Class helps to have constant values used in various class
 *
 * @author ssubashchandar
 */
public class Constants {

    public static final String PRODUCT = "LOAN_CALCULATOR";

    public final static String LOGGER_API = "API";
    public static final String LOGGER_PROBE = "temn.loan.calc.log.show.all";
    public static final String LOGGER_API_PREFIX = "[LOAN_CALCULATOR_API]";
    public static final String LOGGER_REQUEST = "request";
    public static final String LOGGER_RESPONSE = "response";

    public static final String CREATE_RECORD_SUCCESS = "Record created Successfully";
    public static final String UPDATE_RECORD_SUCCESS = "Record updated Successfully";
    public static final String DELETE_RECORD_SUCCESS = "Record deleted Successfully";
    public static final String CREATE_RECORD_FAILURE = "Failed to create Record";
    public static final String UPDATE_RECORD_FAILURE = "Failed to update Record";
    public static final String DELETE_RECORD_FAILURE = "Failed to delete Record";

    public static final Integer FAILURE_API_PROCESS_CODE = 1001;
    public static final String FAILURE_API_PROCESS = "Error while processing the API";
}
