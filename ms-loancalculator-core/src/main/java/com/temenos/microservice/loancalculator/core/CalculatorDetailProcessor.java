package com.temenos.microservice.loancalculator.core;

import com.temenos.logger.Dimension;
import com.temenos.logger.Logger;
import com.temenos.logger.alert.Alert;
import com.temenos.logger.data.LogCode;
import com.temenos.microservice.framework.core.function.Context;
import com.temenos.microservice.loancalculator.entity.LoanCalcDetailEntity;
import com.temenos.microservice.loancalculator.entity.LoanDetailEntity;
import com.temenos.microservice.loancalculator.function.CreateCalculatorDetailInput;
import com.temenos.microservice.loancalculator.function.GetCalculatorDetailInput;
import com.temenos.microservice.loancalculator.function.UpdateCalculatorDetailInput;
import com.temenos.microservice.loancalculator.util.Constants;
import com.temenos.microservice.loancalculator.util.ErrorCodes;
import com.temenos.microservice.loancalculator.util.GenericService;
import com.temenos.microservice.loancalculator.view.CalculatorDetailsBody;
import com.temenos.microservice.loancalculator.view.GetCalculatorDetailsBody;
import com.temenos.microservice.loancalculator.view.LoanCalculatorResponse;

import java.math.BigDecimal;

import static com.temenos.microservice.loancalculator.util.Constants.*;

/**
 * Class will helps the create, update and retrieve record details from Loan Calculator Details
 *
 * @author ssubashchandar
 */
public class CalculatorDetailProcessor {

    private static final Alert alert = Logger.forAlert().forComp(LOGGER_API);

    /**
     * Method helps to create record in Loan Calculator Details
     *
     * @param context
     * @param input
     * @return
     */
    public LoanCalculatorResponse createCalculatorDetails(Context context, CreateCalculatorDetailInput input) {
        LoanCalculatorResponse response = new LoanCalculatorResponse();
        try {
            String loanType = input.getParams().get().getLoanType().get(0);
            LoanCalcDetailEntity loanCalcDetail = (LoanCalcDetailEntity) GenericService.fetchEntity(loanType, LoanCalcDetailEntity.class);
            if (loanCalcDetail != null) {
                // returning error response if record exist with inputted loan type
                response.setAdditionalInfo(ErrorCodes.LC000001_DESC);
                response.setReturnCode(ErrorCodes.LC000001_CODE);
            } else {
                LoanDetailEntity loanDetail = (LoanDetailEntity) GenericService.fetchEntity(loanType, LoanDetailEntity.class);
                if (loanDetail == null) {
                    // returning error response if record exist with inputted loan type
                    response.setAdditionalInfo(ErrorCodes.LC000003_DESC);
                    response.setReturnCode(ErrorCodes.LC000003_CODE);
                } else {
                    loanCalcDetail = new LoanCalcDetailEntity();
                    loanCalcDetail.setLoanType(loanType);
                    CalculatorDetailsBody calcBody = input.getBody().get();
                    saveLoanCalcDetail(loanCalcDetail, calcBody);
                    response.setAdditionalInfo(CREATE_RECORD_SUCCESS);
                }
            }
        } catch (Exception e) {
            alert.prepareError(new LogCode(PRODUCT, LOGGER_API, Dimension.ALERT, FAILURE_API_PROCESS_CODE, FAILURE_API_PROCESS), e).log();
            response.setAdditionalInfo(Constants.CREATE_RECORD_FAILURE);
        }
        return response;
    }


    /**
     * Method helps to update the record in loan calc detail table
     *
     * @param context
     * @param input
     * @return
     */
    public LoanCalculatorResponse updateCalculatorDetails(Context context, UpdateCalculatorDetailInput input) {
        LoanCalculatorResponse response = new LoanCalculatorResponse();
        try {
            String loanType = input.getParams().get().getLoanType().get(0).toString();
            LoanCalcDetailEntity loanCalcDetail = (LoanCalcDetailEntity) GenericService.fetchEntity(loanType, LoanCalcDetailEntity.class);
            if (loanCalcDetail == null) {
                // returning error response if no record with inputted loan type
                response.setAdditionalInfo(ErrorCodes.LC000002_DESC);
                response.setReturnCode(ErrorCodes.LC000002_CODE);
            } else {
                CalculatorDetailsBody calcBody = input.getBody().get();
                saveLoanCalcDetail(loanCalcDetail, calcBody);
                response.setAdditionalInfo(UPDATE_RECORD_SUCCESS);
            }
        } catch (Exception e) {
            alert.prepareError(new LogCode(PRODUCT, LOGGER_API, Dimension.ALERT, FAILURE_API_PROCESS_CODE, FAILURE_API_PROCESS), e).log();
            response.setAdditionalInfo(Constants.UPDATE_RECORD_FAILURE);
        }
        return response;
    }

    /**
     * Method helps to get the calculator details
     *
     * @param context
     * @param input
     * @return
     */
    public GetCalculatorDetailsBody getCalculatorDetails(Context context, GetCalculatorDetailInput input) {
        GetCalculatorDetailsBody response = new GetCalculatorDetailsBody();
        try {
            String loanType = input.getParams().get().getLoanType().get(0).toString();
            LoanCalcDetailEntity loanCalcDetail = (LoanCalcDetailEntity) GenericService.fetchEntity(loanType, LoanCalcDetailEntity.class);
            if (loanCalcDetail == null) {
                // returning error response if no record with inputted loan type
                response.setAdditionalInfo(ErrorCodes.LC000002_DESC);
                response.setReturnCode(ErrorCodes.LC000002_CODE);
            } else {
                LoanDetailEntity loanDetail = (LoanDetailEntity) GenericService.fetchEntity(loanType, LoanDetailEntity.class);
                response.setMaxAmount(loanCalcDetail.getMaxAmount());
                response.setMinAmount(loanCalcDetail.getMinAmount());
                response.setMaxInterestRate(new BigDecimal(loanCalcDetail.getMaxInterestRate()));
                response.setMinInterestRate(new BigDecimal(loanCalcDetail.getMinInterestRate()));
                response.setMaxTermsInMonth(new BigDecimal(loanCalcDetail.getMaxTermsInMonth()));
                response.setMinTermsInMonth(new BigDecimal(loanCalcDetail.getMinTermsInMonth()));
                response.setCurrencySymbol(loanDetail.getCurrencySymbol());
                response.setAdditionalInfo(loanDetail.getDescription());
            }
        } catch (Exception e) {
            alert.prepareError(new LogCode(PRODUCT, LOGGER_API, Dimension.ALERT, FAILURE_API_PROCESS_CODE, FAILURE_API_PROCESS), e).log();
            response.setAdditionalInfo(Constants.FAILURE_API_PROCESS);
        }
        return response;
    }

    /**
     * Method helps to save the record in loan calc details table
     *
     * @param loanCalcDetail
     */
    private void saveLoanCalcDetail(LoanCalcDetailEntity loanCalcDetail, CalculatorDetailsBody calcBody) throws Exception {
        loanCalcDetail.setMaxAmount(calcBody.getMaxAmount());
        loanCalcDetail.setMinAmount(calcBody.getMinAmount());
        loanCalcDetail.setMaxInterestRate(calcBody.getMaxInterestRate().intValue());
        loanCalcDetail.setMinInterestRate(calcBody.getMinInterestRate().intValue());
        loanCalcDetail.setMaxTermsInMonth(calcBody.getMaxTermsInMonth().intValue());
        loanCalcDetail.setMinTermsInMonth(calcBody.getMinTermsInMonth().intValue());
        GenericService.saveEntity(loanCalcDetail);
    }
}
