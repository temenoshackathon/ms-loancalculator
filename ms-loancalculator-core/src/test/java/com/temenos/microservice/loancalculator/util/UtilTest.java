package com.temenos.microservice.loancalculator.util;

import org.junit.Test;

import static org.junit.Assert.fail;

/**
 * Junit class to test the Util methods used in projects
 *
 * @author ssubashchandar
 */
public class UtilTest {

    @Test
    public void getCurrencySymbol() {
        String currency = "USD";
        try {
            String symbol = Util.getCurrencySymbol(currency);
            System.out.println(symbol);
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }
}
