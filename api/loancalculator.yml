openapi: "3.0.0"
info:
  title: Loan Calculator
  description: API to manage the Loan Calculation process
  version: 1.0.0

tags:
  - name: loancalculator
    description: A collection of loan calculator api endpoints

servers:
  - url: http://localhost:8091/ms-loancalculator-api/api/v1.0.0

paths:
  /reference/calculator/{loanType}:
    post:
      summary: Create the record in Loan CalculatorDetail with the basic details such as minimum  principle amount, Interest rate and Terms in month, maximum principle amount, Interest rate and Terms in month to Calculate the Loan amount by selecting the values between the range.
      operationId: createCalculatorDetail
      tags:
        - reference
      parameters:
        - name: loanType
          in: path
          description: Id of Loan CalculatorDetail table
          required: true
          example: HOME
          schema:
            $ref: "#/components/schemas/LoanType"
      requestBody:
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/CreateCalculatorDetailsBody"
      responses:
        '200':
          description: The response from the system
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/LoanCalculatorResponse"

    put:
      summary: Update the record in Loan CalculatorDetail with the basic details such as minimum  principle amount, Interest rate and Terms in month, maximum principle amount, Interest rate and Terms in month to Calculate the Loan amount by selecting the values between the range.
      operationId: updateCalculatorDetail
      tags:
        - reference
      parameters:
        - name: loanType
          in: path
          description: Id of Loan CalculatorDetail table
          required: true
          example: VEHICLE
          schema:
            $ref: "#/components/schemas/LoanType"
      requestBody:
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/CalculatorDetailsBody"
      responses:
        '200':
          description: The response from the system
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/LoanCalculatorResponse"

    get:
      summary: Get details such as minimum  principle amount, Interest rate and Terms in month, maximum principle amount, Interest rate and Terms in month to Calculate the Loan amount.
      operationId: getCalculatorDetail
      tags:
        - reference
      parameters:
        - name: loanType
          in: path
          description: Id of Loan CalculatorDetail table
          required: true
          example: VEHICLE
          schema:
            $ref: "#/components/schemas/LoanType"
      responses:
        '200':
          description: The response from the system
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/GetCalculatorDetailsBody"
  
components:
  schemas:
    LoanType:
      type: string
      enum: [HOME, VEHICLE, PERSONAL]
      
    CalculatorDetailsBody:
      properties:
        description:
          type: string
          description: Description for the Loan type and the payment mode
          example: Home loan
        paymentType:
          type: string
          enum: [MONTH, YEAR]
          description: Payment schedule type either monthly or yearly
          example: YEAR
        minAmount:
          type: number
          description: Minimum amount in selection range
          example: 1000
        maxAmount:
          type: number
          description: Maximum amount in selection range
          example: 99999999
        minInterestRate:
          type: number
          description: Minimum interest rate in selection range
          example: 0
        maxInterestRate:
          type: number
          description: Maximum interest rate in selection range
          example: 15
        minTermsInMonth:
          type: number
          description: Minimum terms in month in selection range
          example: 1
        maxTermsInMonth:
          type: number
          description: Maximum terms in month in selection range
          example: 480
        
      required: [
          paymentType,
          minAmount,
          maxAmount,
          minInterestRate,
          maxInterestRate,
          minTermsInMonth,
          maxTermsInMonth]

    CreateCalculatorDetailsBody:
      allOf: [ 
        $ref: "#/components/schemas/CalculatorDetailsBody"]
      properties:
        currency:
          type: string
          enum: [USD, EUR, SGD, INR]
          description: Base currency using in the bank
      required: [currency]
    
    GetCalculatorDetailsBody:
      allOf: [ 
        $ref: "#/components/schemas/CalculatorDetailsBody"]
      properties:
        currencySymbol:
          type: string
          description: Symbol of the base currecny 
          example: "$"

    LoanCalculatorResponse:
      properties:
        returnCode:
          type: string
          description: The error code if there is a failure
        additionalInfo:
          type: string
          description: The return response message or error message